"""Base

Revision ID: 7cceb63f746e
Revises:
Create Date: 2019-02-13 14:07:03.774616

This file exists to allow downgrading from the first real version
"""

# revision identifiers, used by Alembic.
revision = '7cceb63f746e'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
